var
a:array [1..1024] of longint;
n,i,m,k,j,l,min,minl,p:longint;

procedure swap(var x,y : longint);
var
   t : longint;
begin
   t:=x; x:=y; y:=t;
end;

begin
   readln(m);
   for i:=1 to m do
   begin
      readln(n,k);
      fillchar(a,sizeof(a),0);
      for j:=1 to n do read(a[j]);
      for j:=1 to k do
      begin
	 l:=n-1;
	 while (l>0) and (a[l]>a[l+1]) do dec(l);
	 if l=0 then
	 begin
	    for p:=1 to n do a[p]:=p;
	    continue;
	 end;
	 min:=a[l+1]; minl:=l+1;
	 for p:=l+2 to n do
	    if (a[p]>a[l]) and (a[p]<min) then
	    begin
	       min:=a[p];
	       minl:=p;
	    end;
	 swap(a[l],a[minl]);
	 for p:=l+1 to (n+l+1) div 2 do
	    swap(a[p],a[n-p+l+1]);
      end;
      for j:=1 to n do write(a[j],' ');
      writeln;
   end;
end.

