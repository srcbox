program happy;

var
n,m,p,i,j:longint;
v,w:array [1..25] of longint;
f:array [0..30000] of longint;

function max(x,y:longint):longint;
begin
  if x>y then max:=x else max:=y;
end;

begin
  readln(n,m);
  for i:=1 to m do 
    begin
      readln(v[i],p);
      w[i]:=v[i]*p;
    end;
  for i:=1 to m do 
    for j:=n downto v[i] do 
      f[j]:=max(f[j],f[j-v[i]]+w[i]);
  writeln(f[n]);
end.
 
