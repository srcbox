var
n,i,a,j,max,ans:longint;
check:array [1..20000] of boolean;

begin
   readln(n);
   max:=0;
   fillchar(check,sizeof(check),false);
   for i:=2 to 20000 do
      for j:=2 to 20000 div i do
	 check[i*j]:=true;
   for i:=1 to n do
   begin
      readln(a);
      for j:=a downto 1 do
	 if (not check[j]) and (a mod j=0) then
	 begin
	    if j>max then begin max:=j; ans:=a; end;
	    break;
	 end;
   end;
   writeln(ans);
end.
