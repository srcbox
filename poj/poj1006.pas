var
	p,e,i,d,j,k:longint;

begin
	k:=0;
	repeat
		readln(p,e,i,d);
		inc(k);
		if (p=-1) and (e=-1) and (i=-1) and (d=-1) then break;
		for j:=d+1 to d+21252 do 
			if ((j-p) mod 23=0) and ((j-e) mod 28=0) and ((j-i) mod 33=0) then break;
		writeln('Case ',k,': the next triple peak occurs in ',j-d,' days.');
	until false;
end.