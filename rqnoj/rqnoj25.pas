var
a:array [0..10000] of longint;
n,i,j,k:longint;
ans:int64;

procedure qsort(l,r : longint);
var
   i,j,m : longint;

begin
   i:=l; j:=r;
   m:=a[l+random(r-l+1)];
   repeat
      while a[i]<m do inc(i);
      while a[j]>m do dec(j);
      if i<=j then
      begin
	 a[0]:=a[i]; a[i]:=a[j]; a[j]:=a[0];
	 inc(i); dec(j);
      end;
   until i>j;
   if l<j then qsort(l,j);
   if r>i then qsort(i,r);
end;

begin
   readln(n);
   for i:=1 to n do read(a[i]);
   qsort(1,n);
   for i:=2 to n do
   begin
      a[0]:=a[i]+a[i-1]; a[i]:=0; a[i-1]:=0;
      j:=i+1;
       while(j<n) and (a[j]<a[0]) do inc(j);
      for k:=i-1 to j-2 do a[k]:=a[k+1];
      a[j-1]:=a[0]; inc(ans,a[0]);
   end;
   writeln(ans);
end.
