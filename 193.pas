var
  a:array [0..1000,0..1000] of longint;
  n,m,i,min,j,k,x,y,t,ans:longint;
  v:array [0..1000] of longint;

 begin
   readln(n,m);
   for i:=1 to m do 
     begin
       read(x,y);
       readln(a[x,y]);
       a[y,x]:=a[x,y];
     end;
   v[1]:=1;
   for i:=1 to n-1 do 
     begin
     	min:=maxint; t:=1;
     	for j:=1 to n do 
     	  begin
     	  	if v[i]=1 then 
     	  	  begin
     	  	  	for k:=1 to n do 
     	  	  	  if (a[j,k]<min) and (a[j,k]<>0) then 
     	  	  	    begin
     	  	  	    	min:=a[j,k];
     	  	  	    	t:=j;
     	  	  	    end;
     	  	  end;
     	  end;
     	if min<>maxint then 
     	  begin
     	  	ans:=ans+min;
     	  	v[t]:=1;
     	  end;
     end;
   writeln(ans);
 end.