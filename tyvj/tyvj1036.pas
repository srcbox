var
a:array [0..200000] of longint;
i,n,sum:longint;

procedure qsort(l,r : longint);
var
   i,j,m : longint;

begin
   i:=l; j:=r;
   m:=a[l+random(r-l+1)];
   repeat
      while a[i]<m do inc(i);
      while a[j]>m do dec(j);
      if i<=j then
      begin
	 a[0]:=a[i]; a[i]:=a[j]; a[j]:=a[0];
	 inc(i); dec(j);
      end;
   until i>j;
   if l<j then qsort(l,j);
   if r>i then qsort(i,r);
end;

begin
   readln(n);
   for i:=1 to n do readln(a[i]);
   qsort(1,n);
   i:=1; sum:=1;
   while i<=n do
   begin
      inc(i);
      if a[i]=a[i-1] then inc(sum) else begin writeln(a[i-1],' ',sum); sum:=1; end;
   end;
end.
