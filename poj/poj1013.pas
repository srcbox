var
	n,i,j,p,ansj:longint;
	a:array [1..3,1..3] of string;
	s,ans:string;
	
function check(k,w:longint):boolean;
var
	ch:char;
	i,k1,k2,t,j:longint;
	
begin
	check:=true;
	ch:=chr(ord('A')+k-1);
	for i:=1 to 3 do
		begin
			k:=0;
			for j:=1 to length(a[i,1]) do if a[i,1][j]=ch then inc(k,w) else inc(k);
			for j:=1 to length(a[i,2]) do if a[i,2][j]=ch then dec(k,w) else dec(k);
			if a[i,3]='>>' then if k<=0 then exit(false);
			if a[i,3]='==' then if k<>0 then exit(false);
			if a[i,3]='<<' then if k>=0 then exit(false);
		end;
end;

begin
	readln(n);
	for i:=1 to n do
		begin
			ansj:=0; ans:='';
			for j:=1 to 3 do
				begin
					readln(s);
					p:=pos(' ',s);
					a[j,1]:=copy(s,1,p-1);
					delete(s,1,p);
					p:=pos(' ',s);
					a[j,2]:=copy(s,1,p-1);
					s:=copy(s,p+1,length(s));
					if s='even' then a[j,3]:='==';
					if s='up' then a[j,3]:='>>';
					if s='down' then a[j,3]:='<<';
				end;
			for j:=1 to 12 do
				if check(j,2) then begin ans:='heavy'; ansj:=j; break; end
				else if check(j,0) then begin ans:='light'; ansj:=j; break; end;
			writeln(chr(ord('A')+ansj-1),' is the counterfeit coin and it is ',ans,'.');
		end;
end.
