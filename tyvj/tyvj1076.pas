var
n,i,j,max:longint;
a:array [1..25,1..25] of longint;

procedure dfs(x,y,sum : longint);
begin
   if (x=n) then
   begin
      if sum mod 100>max then max:=sum mod 100;
      exit;
   end;
   dfs(x+1,y,sum+a[x+1,y] mod 100);
   dfs(x+1,y+1,sum+a[x+1,y+1] mod 100);
end; { dfs }

begin
   readln(n);
   for i:=1 to n do
      for j:=1 to i do
	 read(a[i,j]);
   dfs(1,1,a[1,1] mod 100);
   writeln(max);
end.
