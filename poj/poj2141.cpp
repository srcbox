#include <iostream>
#include <map>
#include <string>
#include <cctype>
using namespace std;
int main() {
  map< char,char > changes;
  char ch;
  int i;
  for (i=0;i<26;i++) {
    cin>>ch;
    changes[char(int('a')+i)]=ch;
    changes[char(int('A')+i)]=char(int(ch)-32);
  }
  string st;
  getline(cin,st);
  getline(cin,st);
  for (i=0;i<st.size();i++) 
    if (isalpha(st[i])) cout<<changes[st[i]]; else cout<<st[i];
  return 0;
}
