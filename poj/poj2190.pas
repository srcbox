var
s:string;
sum,k,i:longint;
f:boolean;

begin
   readln(s);
   k:=-1;
   for i:=1 to 10 do
   begin
      if (s[i]<>'?') and (s[i]<>'X') then sum:=(ord(s[i])-ord('0'))*(length(s)-i+1);
	 if s[i]='X' then sum:=sum+10*(length(s)-i+1);
      if s[i]='?' then k:=i;
   end;
   f:=true;
   if k=10 then m:=11 else m:=10;
   for i:=1 to m do
   begin
      if (i*(11-k)+sum) mod 11=0 then
      begin
	 if i<>10 then writeln(i) else writeln('X');
	 f:=false;
      end;
   end;
   if f then writeln(-1);
end.
