var
  a,b,c,d,k,ans:longint;

function min(a,b:longint):longint;
begin
  if a<b then exit(a) else exit(b);
end;

begin
  assign(input,'cake.in'); reset(input);
  assign(output,'cake.out'); rewrite(output);
  readln(a,b,c,d);
  k:=min(a,d);
  inc(ans,k);
  d:=0; a:=a-ans;
  k:=min(b,c);
  inc(ans,k);
  b:=b-k; c:=c-k;
  k:=min(a div 2,c);
  inc(ans,k);
  c:=0; a:=a-k*2;
  k:=min(a,b div 2);
  inc(ans,k);
  b:=b-k*2; a:=a-k;
  k:=min(a div 3,b);
  inc(ans,k);
  b:=0; a:=a-k*3;
  k:=a div 5;
  inc(ans,k);
  writeln(ans);
  close(input); close(output);
end.