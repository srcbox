var
a:array [1..12] of longint;
sum,bank,i:longint;
f:boolean;

begin
   for i:=1 to 12 do read(a[i]);
   f:=true;
   for i:=1 to 12 do
   begin
      inc(sum,300);
      dec(sum,a[i]);
      if sum<0 then begin f:=false; break; end;
      inc(bank,sum div 100*100);
      sum:=sum mod 100;
   end;
   if not f then writeln('-',i) else writeln(bank*1.2+sum:0:0);
end.
