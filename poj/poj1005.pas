var
	n,i,year:longint;
	x,y,distance:double;

function r(year:longint):double;
begin
	exit(sqrt(100*year/pi));
end;

begin
	readln(n);
	for i:=1 to n do 
		begin
			readln(x,y);
			distance:=sqrt(sqr(x)+sqr(y));
			year:=1;
			while distance>=r(year) do inc(year);
			writeln('Property ',i,': This property will begin eroding in year ',year,'.');
		end;
	writeln('END OF OUTPUT.')
end.