var
  n,m,x,i,j,k,h,l,r,ans,a,b,p,q:longint;
  hash:array [1..3000,1..3000] of longint;
  f:boolean;

begin
  assign(input,'base.in'); reset(input);
  assign(output,'base.out'); rewrite(output);
  readln(n,m,x);
  for i:=1 to x do
    begin
      readln(a,b);
      hash[a,b]:=1;
    end;
  for i:=1 to n do
    for j:=1 to m do
      if hash[i,j]<>1 then
        begin
          k:=i-1; f:=true;
          while (k>0) and (hash[k,j]=0) do dec(k);
          h:=i-k;
          for l:=j-1 downto 1 do
            begin
              for q:=i downto k+1 do
                if hash[q,l]=1 then begin f:=false; break; end;
              if not f then break;
            end;
          if not f then inc(l);
          if l=0 then l:=1; f:=true;
          for r:=j+1 to m do
            begin
              for q:=i downto k+1 do
                if hash[q,r]=1 then begin f:=false; break; end;
              if not f then break;
            end;
          if not f then dec(r);
          if r>m then r:=m;
          if (r-l+1)*h>ans then ans:=(r-l+1)*h;
        end;
  writeln(ans);
  close(input); close(output);
end.
