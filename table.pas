var
	a,b,group:array [0..500,0..500] of longint;
	f:array [0..500] of boolean;
	g:array [0..500,0..2] of longint;
	n,i,j,k,j1,t,t1,numofgroup,p,q,ans:longint;

function min(a,b:longint):longint;
begin
	if a<b then exit(a) else exit(b);
end;

function max(a,b:longint):longint;
begin
	if a>b then exit(a) else exit(b);
end;

procedure floyd;
var
	i,j,k:longint;

begin
	for k:=1 to n do
		for i:=1 to n do
			for j:=1 to n do
				b[i,j]:=min(b[i,j],b[i,k]+b[k,j]);
end;

procedure floodfill(k,num,sum:longint);
var
	i:longint;

begin
	f[k]:=true; inc(group[num,0]); group[num,sum]:=k;
	for i:=1 to n do
		if (i<>k) and (f[i]=false) and (a[k,i]=1) then
			begin
				floodfill(i,num,sum+1);
			end;
end;

procedure sort;
var
	i,j:longint;

begin
	for i:=1 to numofgroup-1 do
		for j:=i+1 to numofgroup do
			if g[i,1]>g[j,1] then
				begin
					g[0]:=g[i]; g[i]:=g[j]; g[j]:=g[0];
                    group[0]:=group[i]; group[i]:=group[j]; group[j]:=group[0];
				end;
end;

procedure insertsort(k:longint);
var
	i,j:longint;

begin
	g[0]:=g[k];
	g[k,1]:=0;
	j:=k;
	while (g[j,1]<g[0,1]) and (j<numofgroup) do inc(j);
	for i:=1 to j-2 do g[i]:=g[i+1];
	g[j-1]:=g[0];
end;

begin
	readln(n);
	readln(p,q);
	while (p<>0) and (q<>0) do
		begin
			a[p,q]:=1;
			a[q,p]:=1;
			readln(p,q);
		end;
	b:=a;
	for i:=1 to n do
		for j:=1 to n do
			if b[i,j]=0 then b[i,j]:=maxint;
        for i:=1 to n do b[i,i]:=0;
	floyd;
	for i:=1 to n do
		if f[i]=false then
			begin
				inc(numofgroup);
				floodfill(i,numofgroup,1);
			end;
	for i:=1 to numofgroup do
		begin
			t1:=maxint; j1:=group[i,1];
			for j:=1 to group[i,0] do
				begin
					t:=0;
					for k:=1 to group[i,0] do
						t:=max(b[group[i,j],group[i,k]],t);
					if t<t1 then begin t1:=t; j1:=j; end;
				end;
			g[i,1]:=t1; g[i,2]:=group[i,j1];
		end;
	sort;
	for i:=2 to numofgroup do
		begin
			writeln(g[i-1,2],' ',g[i,2],' ',n+i-1);
			g[i,1]:=max(g[i,1],g[i-1,1])+1; g[i,2]:=n+i-1;
            if i<>numofgroup then insertsort(i);
		end;
    writeln(g[numofgroup,1]);
end.
