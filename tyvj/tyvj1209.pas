var
n,i,j,max:longint;
a,f:array [0..1000] of longint;
ch:char;

begin
   n:=1;
   while not eoln do
   begin
      read(ch);
      if ch<>',' then a[n]:=a[n]*10+ord(ch)-ord('0') else inc(n);
   end;
  max:=1; for i:=1 to n do f[i]:=1;
   for i:=1 to n do
     for j:=i-1 downto 1 do
       if a[j]>a[i] then
         begin
              if f[i]<f[j]+1 then f[i]:=f[j]+1;
              if f[i]>max then max:=f[i];
         end;
   write(max,',');
   for i:=1 to n do f[i]:=1; max:=1;
    for i:=1 to n do
       for j:=i-1 downto 1 do
        if a[j]<a[i] then
         begin
              if f[i]<f[j]+1 then f[i]:=f[j]+1;
              if f[i]>max then max:=f[i];
         end;
   writeln(max-1);
end.
