type
  arr=array [0..80,0..2] of longint;

var
  a,b,c:arr;
  i,j,lena,lenb,lenc,lenp:longint;
  st:string;

function len(a:longint):string;
var
  s:string;
  i:longint;

begin
  str(abs(a),s);
  if s='1' then exit('');
  for i:=1 to length(s) do s[i]:=' ';
  exit(s);
end;

function getsum(st:string; var k:longint):longint;
begin
  getsum:=0;
  while (k<=length(st)) and (st[k] in ['0'..'9']) do begin getsum:=getsum*10+ord(st[k])-ord('0'); inc(k); end;
end;

procedure trans(st:string; var p:arr);
var
  i,k1,k2,k,t:longint;
  q:array [0..80] of string;
  f:boolean;

begin
  t:=0;
  for i:=1 to 80 do q[i]:='';
  i:=0;
  for j:=1 to length(st) do
    begin
      if (st[j]='+') or (st[j]='-') then inc(i);
      q[i]:=q[i]+st[j];
    end;
  lenp:=i;
  for j:=1 to i do
    if q[j]<>'' then
      begin
        if q[j][1]='-' then p[j,0]:=-1 else p[j,0]:=1;
        k:=2; t:=getsum(q[j],k);
        if t<>0 then p[j,0]:=p[j,0]*t;
        if k>length(q[j]) then continue;
        if q[j][k]='x' then begin inc(k); t:=getsum(q[j],k); if t=0 then t:=1; p[j,1]:=t; end
        else if q[j][k]='y' then begin inc(k); t:=getsum(q[j],k); if t=0 then t:=1; p[j,2]:=t; end;
        if k<=length(q[j]) then
          if q[j][k]='x' then begin inc(k); t:=getsum(q[j],k); if t=0 then t:=1; p[j,1]:=t; end
          else if q[j][k]='y' then begin inc(k); t:=getsum(q[j],k); if t=0 then t:=1; p[j,2]:=t; end;
      end;
end;

function next(j:longint):longint;
var
  i:longint;

begin
  next:=j+1;
  for i:=j+1 to lenc do if c[i,0]=0 then inc(next) else break;
end;

begin
  assign(input,'multiply.dat'); reset(input);
  assign(output,'multiply.out'); rewrite(output);
  readln(st);
  if st='0' then begin writeln; writeln(0); close(input); close(output); halt; end;
  if (st[1] in ['x'..'y']) or (st[1] in ['0'..'9']) then st:='+'+st;
  trans(st,a);
  lena:=lenp; lenp:=0;
  readln(st);
  if st='0' then begin writeln; writeln(0); close(input); close(output); halt; end;
  if (st[1] in ['x'..'y']) or (st[1] in ['0'..'9']) then st:='+'+st;
  trans(st,b);
  lenb:=lenp;
  for i:=1 to lena do
	for j:=1 to lenb do
	  begin
	    c[(i-1)*lenb+j,0]:=a[i,0]*b[j,0];
		c[(i-1)*lenb+j,1]:=a[i,1]+b[j,1];
		c[(i-1)*lenb+j,2]:=a[i,2]+b[j,2];
	  end;
  lenc:=lena*lenb;
  for i:=1 to lena*lenb do
    for j:=1 to i-1 do
	  if (i<>j) and (c[i,1]=c[j,1]) and (c[i,2]=c[j,2]) then
	    begin
		  inc(c[i,0],c[j,0]);
		  c[j,0]:=0; c[j,1]:=0; c[j,2]:=0;// dec(lenc);
		end;

  for i:=1 to lenc-1 do
    for j:=i+1 to lenc do
	  if (c[i,1]<c[j,1]) or ((c[i,1]=c[j,1]) and (c[i,2]>c[j,2])) then
	    begin
		  c[0]:=c[i]; c[i]:=c[j]; c[j]:=c[0];
		end;
  if c[1,0]<0 then write(' ');
  for i:=1 to lenc do
    if c[i,0]<>0 then
	  begin
                write(len(c[i,0]));
                if (abs(c[i,0])=1) and (c[i,1]=0) and (c[i,2]=0) then write(' ');
                if c[i,1]<>0 then
                  begin
                    write(' ');
                    if c[i,1]<>1 then write(c[i,1]);
                  end;
                if c[i,2]<>0 then
                  begin
                    write(' ');
                    if c[i,2]<>1 then write(c[i,2]);
                  end;
		if c[next(i),0]<>0 then write('   ');
	  end;
  writeln;
  if c[1,0]<>0 then
    begin
      if c[1,0]<0 then write('-');
    //     if abs(c[1,0])<>1 then write(abs(c[1,0]));
    end;
  for i:=1 to lenc do
    if c[i,0]<>0 then
	  begin
	        if (abs(c[i,0])<>1) or ((abs(c[i,0])=1) and (c[i,1]=0) and (c[i,2]=0)) then write(abs(c[i,0]));
		if c[i,1]<>0 then write('x',len(c[i,1]));
                if c[i,2]<>0 then write('y',len(c[i,2]));
                write(' ');
                if c[next(i),0]>0 then write('+ ') else
                if c[next(i),0]<0 then write('- ');
      end;
  close(input); close(output);
end.
