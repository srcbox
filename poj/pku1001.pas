const
	maxn=150;

var
	r:array [1..6] of longint;
	c,d:array [1..maxn] of longint;
	s,st:string;
	p,f,i,n,j,k:longint;

begin
	readln(st);
	s:=copy(st,1,6);
	p:=pos('.',s);
	if p=0 then p:=6;
  f:=6-p;
	delete(s,p,1);
	while length(s)<6 do s:='0'+s;
	for i:=1 to 6 do r[i]:=ord(s[i])-ord('0');
	delete(st,1,7);
	val(st,n);
	c[maxn]:=1;
	for i:=1 to n do
	  begin
			for k:=6 downto 1 do
   			for j:=maxn downto 1 do
   				begin
						d[i+j-1]:=d[i+j-1]+c[j]*r[k];
						d[i+j]:=d[i+j-1] div 10;
						d[i+j-1]:=d[i+j-1] mod 10;
					end;
			c:=d;
			fillchar(d,sizeof(d),0);
		end;
	f:=trunc(exp(n*ln(f)));
	j:=1;
	while c[j]=0 do inc(j);
	for i:=j to maxn-f do write(c[i]);
	write('.');
	for i:=maxn-f+1 to maxn do write(c[i]);
  writeln;
end.

