var
  x,y,z,p:longint;
  
begin
  assign(input,'sector.dat'); reset(input);
  assign(output,'sector.out'); rewrite(output);
  readln(x,y,z,p);
  //p:=p+1;
  if p>=x*y*z then writeln('0 0 0')
                else begin
				         writeln(p div z mod x,' ',p div z div x,' ',p mod z+1)
					   end;
	close(input); close(output);
end.