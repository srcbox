uses graph;
var
  sht,cnt:array [0..1000] of longint;
  num:array [0..1000,1..2] of longint;
  r,c:longint;
  fail:boolean;

procedure draw;
var
  gd,gm,i,j:longint;

begin
  gd:=0;
  initgraph(gd,gm,'E:\bp\bgi');
  for i:=1 to c do
    for j:=1 to r do
      if (num[i,1]=j) or (num[i,2]=j) then
        bar(i*10,j*10,i*10+10,j*10+10)
      else
        rectangle(i*10,j*10,i*10+10,j*10+10);
  readln;
  closegraph;
end;

procedure run;
var
  i,j,l,min,mw:longint;

begin
  fail:=false;
  for i:=1 to c do
    begin
      if i<=r then
        begin
          min:=maxint;
          for j:=1 to r do
            if cnt[j]<min then begin min:=cnt[j]; mw:=j; end;
        end
      else
        for j:=1 to c do if sht[j]=0 then begin mw:=num[j,1]; break; end;
      if min=0 then begin fail:=true; break; end
      else begin
        l:=1;
        while (l<=c) do
          begin
            if (sht[l]=0) and ((num[l,1]=mw) or (num[l,2]=mw)) then
              begin
                sht[l]:=mw;
                dec(cnt[num[l,1]]);
                dec(cnt[num[l,2]]);
                cnt[mw]:=maxint;
                break;
              end;
            inc(l);
          end;
        if sht[l]=0 then
          writeln('Wrong');
end;

begin
  readln(r,c);
  for i:=1 to c do
    begin
      readln(num[i,1],num[i,2]);
      inc(cnt[num[i,1]]);
      inc(cnt[num[i,2]]);
    end;
  draw;
  if fail then writeln('NO')
  else for i:=1 to c do write(sht[i],' ');
end.
