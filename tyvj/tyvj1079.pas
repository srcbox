var
n,i,j:longint;
a,f:array [0..25,0..25] of longint;

function max(x,y : longint):longint;
begin
   if x>y then exit(x) else exit(y);
end; { max }

begin
   readln(n);
   for i:=1 to n do
      for j:=1 to i do
	 read(a[i,j]);
   for i:=1 to n div 2 do inc(f[n div 2,n div 2],a[i,i]);
   for i:=n div 2 +1 to n do
      for j:=n div 2 to i do
	 f[i,j]:=max(f[i-1,j],f[i-1,j-1])+a[i,j];
   for i:=1 to n do
      f[n,n]:=max(f[n,n],f[n,i]);
   writeln(f[n,n]);
end.
