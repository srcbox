package hello

import (
    "fmt"
    "http"
)

func init() {
    http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
    var a[1000] int
    var i,j int
    for i=2; i<1000; i++ {
	for j=i+1; j<=1000; j++ {
 	  if (j%i == 0) {
		a[j]=1
	  }
	}
    }
    for i=2; i<=1000; i++ {
	if (a[i]!=1) {
           fmt.Fprint(w,i)
  	   fmt.Fprint(w," ")
	}
    }
}
