const
way : array [1..8,1..2] of longint=((-2,-1),(-2,1),(-1,2),(1,2),
				  (2,1),(2,-1),(1,-2),(-1,-2));
var
   a				    : array [-1..150,-1..150] of char;
   queue			    : array [0..100000] of record
					 x,y		    : longint;
				      end;
   father			    : array [0..100000] of longint;
   endqueue			    : record
					 x,y : longint; 
				      end;   
   i,j,m,n,head,tail,ans	    : longint;

function check(x,y : longint):boolean;
begin
   check:=false;
   if (a[x,y]='.') then exit(true);
end;

begin
   readln(m,n);
   for i:=1 to n do
   begin
      for j:=1 to m do
      begin
	 read(a[i,j]);
	 if a[i,j]='K' then begin queue[1].x:=i; queue[1].y:=j; a[i,j]:='.'; end;
	 if a[i,j]='H' then begin endqueue.x:=i; endqueue.y:=j; a[i,j]:='.'; end;
      readln;
      end;
   end;
   head:=0; tail:=1;
   while head<tail do
   begin
      inc(head);
      for i:=1 to 8 do
	 if check(queue[head].x+way[i,1],queue[head].y+way[i,2]) then
	 begin
	    inc(tail);
	    queue[tail].x:=queue[head].x+way[i,1];
	    queue[tail].y:=queue[head].y+way[i,2];
	    father[tail]:=head;
	    if (queue[tail].x=endqueue.x) and (queue[tail].y=endqueue.y) then break;
	 end;
   end;
   i:=tail; ans:=0;
   while i<>1 do
   begin
      i:=father[tail];
      inc(ans);
   end;
end.
