var
a:array ['A'..'Z'] of char;
b:array ['a'..'z'] of char;
i:longint;
ch:char;
s:string;

begin
   for ch:='a' to 'z' do
   begin
      read(b[ch]);
      a[chr(ord(ch)-32)]:=chr(ord(b[ch])-32);
   end;
   readln;
   readln(s);
   for i:=1 to length(s) do
      if s[i]<>' ' then
      begin
	 if s[i] in ['A'..'Z'] then write(a[s[i]]);
	 if s[i] in ['a'..'z'] then write(b[s[i]]);
      end
      else write(s[i]);
end.
