var
  a:array [0..100] of string;
  v:array ['A'..'Z'] of longint;
  b:array [0..100] of longint;
  p,k:longint;
  s:string;

function solve(s:string):longint;
var
  i:longint;

begin
  if s[1] in ['A'..'Z'] then
    begin
      if s[2]='+' then exit(v[s[1]]+v[s[3]]);
      if s[2]='>' then
        if v[s[1]]>v[s[3]] then exit(1) else exit(0);
      exit;
    end;
  val(s,solve);
end;

procedure goto1(line:longint);
begin
  if a[line]='STOP' then halt;
  if b[line]=1 then
      v[a[line][1]]:=solve(copy(a[line],pos('=',a[line])+1,length(a[line])))
  else
  if b[line]=2 then
      writeln(a[line],'=',v[a[line][1]])
  else
  if b[line]=3 then
      goto1(solve(a[line]))
  else
  if b[line]=4 then
      if solve(a[line])=0 then goto1(line+2);
  goto1(line+1);
end;

begin
  assign(input,'tbasic.in'); reset(input);
  while not eof do
    begin
      read(k);
      readln(a[k]);
      while a[k][1]=' ' do delete(a[k],1,1);
      p:=pos(' ',a[k]);
      s:=copy(a[k],1,p-1);
      delete(a[k],1,p-1);
      if s='LET' then b[k]:=1
      else if s='PRINT' then b[k]:=2
      else if s='GOTO' then b[k]:=3
      else if s='IF' then b[k]:=4
      else if s='STOP' then b[k]:=5;
      while a[k][1]=' ' do delete(a[k],1,1);
    end;
  goto1(1);
end.