var
a,b,s:string;
i,j,k,p:longint;

function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;

begin
   readln(a);
   readln(b);
   k:=max(length(a),length(b));
   for i:=k downto 1 do
   begin
      s:=copy(a,1,i);
      p:=pos(s,b);
      if (p>0) and (p+i-1=length(b)) then begin writeln(i); halt; end;
      s:=copy(b,1,i);
      p:=pos(s,a);
      if (p>0) and (p+i-1=length(a)) then begin writeln(i); halt; end;
   end;
   writeln(0);
end.
