var
	a:array [0..10000] of longint;
	data:array [0..1000,1..2] of longint;
	f:array [0..1000] of longint;
	n,i,j,max1,maxk1,max2,maxk2,ans,k:longint;

procedure qsort(l,r:longint);
var
	i,j,m:longint;

begin
	i:=l; j:=r;
	m:=data[(l+r) div 2,1];
	repeat
		while data[i,1]<m do inc(i);
		while data[j,1]>m do dec(j);
		if i<=j then
			begin
				data[0]:=data[i]; data[i]:=data[j]; data[j]:=data[0];
				inc(i); dec(j);
			end;
	until i>j;
	if l<j then qsort(l,j);
	if r>i then qsort(i,r);
end;

begin
	readln(n);
	for i:=1 to n do readln(data[i,1],data[i,2]);
	qsort(1,n);
	for i:=1 to n-1 do
	  if data[i,1]=data[i+1,1] then
	    begin
	    	if data[i,2]<data[i+1,2] then
	    		begin
	    			data[i+1,1]:=maxint; data[i+1,2]:=maxint;
	    		end
	    	else
	    		begin
	    			data[i,1]:=maxint; data[i,2]:=maxint;
	    		end;
	    end;
	qsort(1,n);
	while data[n,1]=maxint do dec(n);
	for i:=1 to n do
		for j:=data[i,1] to data[i,2] do
			inc(a[j]);
	fillchar(f,sizeof(f),0);
	for i:=1 to n do
        begin
            if f[i]>=2 then continue;
            max1:=a[data[i,1]]; maxk1:=data[i,1];
            max2:=a[data[i,2]]; maxk1:=data[i,2];
            for k:=data[i,1]+1 to data[i,2]-1 do
		    if a[k]>max1 then begin max2:=max1; maxk2:=maxk1; max1:=a[k]; maxk1:=k; end
            else if a[k]>max2 then begin max2:=a[k]; maxk2:=k; end;
            if f[i]=0 then
                begin
                    inc(ans); a[maxk2]:=0;
                    j:=i+1;
                    while (maxk2>=data[j,1]) and (maxk2<=data[j,2]) do
                        begin
                            inc(f[j]); inc(j);
                        end;
                end;
            inc(ans); a[maxk1]:=0;
            j:=i+1;
            while (maxk1>=data[j,1]) and (maxk1<=data[j,2]) do
                begin
                    inc(f[j]); inc(j);
                end;
        end;
	writeln(ans-1);
end.
