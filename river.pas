var
  n,i,time,j:longint;
  last,now:array [-1001..1001] of boolean;
  a,b:array [0..1001] of longint;
  f:boolean;

function up(i,time:longint):boolean;
begin
  time:=time mod (a[i]+b[i]);
  if (time>=1) and (time<=a[i]) then exit(true) else exit(false);
end;

begin
  readln(n);
  for i:=1 to n do readln(a[i],b[i]);
  last[0]:=true;
  repeat
    f:=false;
    inc(time);
    if last[n+1] then begin writeln(time-1); close(input); close(output); halt; end;
    fillchar(now,sizeof(now),false);
    for i:=0 to n+1 do
      if (i=0) or (i=n+1) or (up(i,time)) then
        for j:=i-5 to i+5 do
          if (last[j]) and (j>=0) and (j<=n+1) then
            begin
              f:=true;
              now[i]:=true;
              break;
            end;
    last:=now;
  until (time>2520) or (not f);
  if not f then writeln('NO');
end.
