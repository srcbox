var
	a:array [0..100,0..100] of char;
	b:array [0..100,1..2] of longint;
	x,y,i,j,m,n,ans:longint;

procedure dfs(x,y:longint);
var
	i,j:longint;

begin
	if a[x,y]='#' then exit;
	if (x=0) or (x>m) or (y=0) or (y>n) then exit;
	a[x,y]:='#';
	if a[x,y-1]='@' then dfs(x,y-1);
    if a[x-1,y]='@' then dfs(x-1,y);
	if a[x,y+1]='@' then dfs(x,y+1);
	if a[x+1,y]='@' then dfs(x+1,y);
	if a[x-1,y-1]='@' then dfs(x-1,y-1);
    if a[x-1,y+1]='@' then dfs(x-1,y+1);
	if a[x+1,y+1]='@' then dfs(x+1,y+1);
	if a[x+1,y-1]='@' then dfs(x+1,y-1);
end;
begin
	while true do 
		begin
			ans:=0;
			for i:=1 to 100 do for j:=1 to 100 do a[i,j]:='$';
			readln(m,n);
			if (m=0) and (n=0) then break;
			for i:=1 to m do 
				begin
					for j:=1 to n do 
						read(a[i,j]);
					readln;
				end;
			for i:=1 to m do 
				for j:=1 to n do 
					if a[i,j]='@' then
						begin
							dfs(i,j);
							inc(ans);
						end;
			writeln(ans);
		end;
end.