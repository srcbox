var
i:longint;
n,sum:double;

begin
   read(n);
   while n<>0 do
   begin
      i:=2;
      sum:=1/i;
      while sum<n do
      begin
	 inc(i);
	 sum:=sum+1/i;
      end;
      writeln(i-1,' card(s)');
      read(n);
   end;
end.
