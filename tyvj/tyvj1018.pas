var
n,k,i:longint;
sum:qword;
s:string;

begin
   readln(n,k);
   sum:=1;
   for i:=1 to n do
      sum:=sum*i;
   str(sum,s);
   while s[length(s)]='0' do delete(s,length(s),1);
   writeln(copy(s,length(s)-k+1,length(s)));
end.
